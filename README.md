# OpenML dataset: Meta-Album--PLK--(Micro)

https://www.openml.org/d/43933

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Plankton dataset from WHOI

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43933) of an [OpenML dataset](https://www.openml.org/d/43933). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43933/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43933/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43933/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

